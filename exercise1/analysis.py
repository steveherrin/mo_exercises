#!/usr/bin/env python
"""
Run the analysis

Usage:
  analysis.py <input_pickle>...

Options:
  -h --help     Show this
"""
import functools
import operator
import os
import pickle

import docopt
from matplotlib import pyplot
import numpy
import scipy.stats
from scipy.special import xlogy


def entropy(allele_freqs):
    """
    Compute the information entropy of variants

    The idea is that we can compute the probabilities
    for all genotypes (0, 1, or 2 variants) by computing
    p(genotype) = sum over p(genotype|population) * p(population)

    The entropy depends on the distribution of populations.
    Intuitively, think about a variant that has a 50% allele
    frequency in africans, but 0% in other populations. This
    variant will tell us more if our population is mostly
    african.
    
    I'm assuming equal-sized populations, but in reality
    you'd want to use a population distribution similar to
    what you plan to study
    """

    # sanity checks
    assert len(allele_freqs.shape) == 2 and allele_freqs.shape[1] == 3

    population_probs = numpy.array([1, 1, 1])  # assume each population is equally represented
    population_probs = population_probs / numpy.sum(population_probs)  # normalize probabilities to 1
    population_probs = numpy.tile(population_probs, (allele_freqs.shape[0], 1))  # one copy per variant

    # compute probabilities of different variant counts
    p_2_given_pop = allele_freqs * allele_freqs * population_probs
    p_1_given_pop = allele_freqs * (1 - allele_freqs) * population_probs
    p_0_given_pop = (1 - allele_freqs) * (1 - allele_freqs) * population_probs
    # use the definition of information entropy to get it per population
    entropy = (
        xlogy(p_2_given_pop, p_2_given_pop) +
        xlogy(p_1_given_pop, p_1_given_pop) +
        xlogy(p_0_given_pop, p_0_given_pop)
    )
    # sum columns to get total entropy per variant
    return -numpy.sum(entropy, axis=1)


def similarity_between_populations(common_variants, allele_freqs):
    """
    Compute how similar populations are to the first population, per chromosome

    Arguments are the results of make_allele_freq_array

    Parameters:
      common_variants: list
      allele_freqs: numpy array

    Returns:
      {chromosome: [similarity between population 0 and itself, between population 0 and population 1, ...]}
    """
    chromosomes = {chr for (chr, _) in common_variants}
    results = {}
    for chromosome in chromosomes:
        # per chromosome, make vectors of the allele frequencies and compute correlation pairwise
        chr_freqs = numpy.array([row for row, (chr, _) in zip(allele_freqs, common_variants) if chr == chromosome])
        results[chromosome] = [scipy.stats.pearsonr(chr_freqs[:, 0], chr_freqs[:, i])[0] for i in range(chr_freqs.shape[1])]
    return results


def _chromosome_to_number(chromosome):
    """
    Convert a chromosome string to a number, or 23 if 'X'

    Parameters:
      chromosome: string

    Returns:
      int
    """
    if chromosome == 'X':
        return 23
    else:
        return int(chromosome)


def plot_similarity_between_populations(chromosome_to_similarities, out_file, labels=None):
    """
    Make a plot of how similar populations are, per chromosome

    Arguments are the results of similarity_between_populations

    Parameters:
      chromosome_to_similarities: {chromosome: [similarity values]}
      out_file: path to save the plot

    Returns:
      None
    """
    fig = pyplot.figure()
    ax = pyplot.subplot(111)

    # we want to plot them in numerical order
    chromosomes = sorted(chromosome_to_similarities.keys(), key=_chromosome_to_number)
    # if no labels provided, generate some
    labels = labels or [f'population {i}' for i in range(len(chromosome_to_similarities[chromosomes[0]]))]

    x_values = [_chromosome_to_number(chr) for chr in chromosomes]
    # extract the similarity between the 0th and ith populations
    for i, label in enumerate(labels):
        y_values = numpy.empty(len(chromosomes))
        for j, chromosome in enumerate(chromosomes):
            # we use an index to make sure the chromosome order is correct
            y_values[j] = chromosome_to_similarities[chromosome][i]

        pyplot.scatter(x_values, y_values, label=labels[i])
    pyplot.title(f'Similarity to {labels[0]}')
    ax.legend()
    fig.savefig(out_file)


def make_allele_freq_array(pop_to_allele_freqs):
    """
    Array of allele frequencies, one row per variant and one column per population

    Parameters:
      pop_to_allele_freqs: dict
        {population: {(chromosome, position): allele_frequency, ...}}

    Returns:
      tuple of (list of common variants, numpy array of allele_frequencies)
    """
    common_variants = functools.reduce(operator.and_, (set(afs.keys()) for afs in pop_to_allele_freqs.values()))
    common_variants = sorted(common_variants)  # variants in all populations

    # we'll fill this in
    allele_freqs = numpy.empty((len(common_variants), len(pop_to_allele_freqs)))

    # iterate over all populations and all variants and fill in the array
    for i, (population, pop_freqs) in enumerate(sorted(pop_to_allele_freqs.items(), key=operator.itemgetter(0))):
        for j, variant in enumerate(common_variants):
            allele_freqs[j, i] = pop_freqs[variant]

    return common_variants, allele_freqs


def load(filenames):
    """
    Load the picked data from ingest.py

    Parameters:
      filenames: list

    Returns:
      {population: {(chromosome, position): allele_frequency, ...}}
    """
    population_to_allele_freqs = {}
    for filename in filenames:
        population_name = os.path.basename(filename).split('.')[0]

        with open(filename, 'rb') as f:
            population_to_allele_freqs[population_name] = pickle.load(f)

    return population_to_allele_freqs


def main(pickle_filenames):
    """
    Run the main analysis

    Roughly, load the pickles from ingest.py
    Make a nice numpy array with the common variants
    Compute and rank variants by entropy
    Compute and plot similarity per chromosome
    """
    population_to_allele_freqs = load(pickle_filenames)

    common_variants, allele_freqs = make_allele_freq_array(population_to_allele_freqs)

    variant_entropys = entropy(allele_freqs)
    best_variant_order = numpy.argsort(-variant_entropys)  # want the ones with the most entropy
    best_variants = [common_variants[i] for i in best_variant_order]

    with open('highest_entropy_variants.tsv', 'w') as f:
        f.write('chromosome\tposition\n')
        for variant in best_variants:
            f.write(f'{variant[0]}\t{variant[1]}\n')
    print('produced highest_entropy_variants.tsv')

    s = similarity_between_populations(common_variants, allele_freqs)
    plot_similarity_between_populations(s, 'similarity_by_chromosome.png', labels=sorted(population_to_allele_freqs.keys()))
    print('produced similarity_by_chromosome.png')


if __name__ == '__main__':
    args = docopt.docopt(__doc__)
    main(args['<input_pickle>'])
