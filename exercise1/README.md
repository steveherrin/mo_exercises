# Analysis Code Exercise

This code analyzes some allele frequency data from the 1000 Genomes
Project for the AFR, ASN, and EUR populations. It produces two results:

## Entropy Analysis

Suppose we wanted to build a genetic fingerprint. To do this, we'd
want to pick a set of variants that show variation in a wide variety
of populations. If an allele frequency is 50% in most populations,
for example, then using it in a fingerprinting algorithm will
distinguish about half of the individuals from each other.

The code here looks at the allele frequency data for the three
populations and determines which would be most useful in a
fingerprinting algorithm. It does this by computing the information
entropy of each variant, and then ranking all the variants.

## Similarity Analysis

This produces a visualization of how similar the populations are on
different chromosomes. To get this, it computes the correlation
between the allele frequencies on each chromosome between the
populations in a pairwise fashion.

# Running It

This was developed using Python 3.7. Older Python 3 versions might
work as well. I've made no attempt at Python 2 compatibility.

A `Makefile` provides an easy way to run the analysis, and handles
dependencies between the stages of analysis.

`make analysis` should run the full analysis from scratch

`make test` runs some tests I developed to check tricky bits of
logic, but they are by no means exhaustive.
