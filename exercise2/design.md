# Introduction

The EU's General Data Protection Regulation (GDPR) allows, among
other things, an individual to revoke their consent to use their
data. In a research and development (R&D) setting, this requires
us to delete as much of that individual's data as possible and to
stop using that individual's data in ongoing studies. However, we
are allowed to retain data used for a scientific publication.

This document outlines a system to allow the tracking of an
individual's data through different datasets in an R&D system, to
allow for deletion upon request (or allow for communication about
publications).

# Existing systems and assumptions

In this document, I assume an R&D environment has already been
created, and it allows researchers to interactively work on data.
They have some ability to run scipts or utility code. Datasets are
predominantly stored in shared storage that supports a directory
structure (like a shared file system, or AWS S3), but may also be
stored in systems like databases.

I've assumed there's some system for running scripts in an automated,
periodic fashion. This could be as simple as a permanently running
machine with `cron`, or it could be a job in an automation
system like Jenkins, or it could use a cloud provder's system like
AWS Lambda.

I've assumed that researchers are cooperative and willing to work
with the tracking system, provided it isn't too cumbersome to
use. I'm not assuming they won't make mistakes, but it will require
good faith attempts at compliance.

# Broad Overview

The data tracking system tracks how consists of a few main
components:

1. a database used to keep track of datasets and individuals
2. tools to keep track of datasets and react to deletion requests
3. automated systems to aid tracking and deletions, and to
   identify untracked datasets

To allow for maximum flexibility in what datasets can be used,
deletions proceed in two stages. First, an individual requests a
deletion and the system records that. Then, for each dataset, the
deletion must be confirmed by whatever process handles the deletion.
This allows a researcher at an early stage of analysis to manually
manage their dataset, for example.

A central database will be used to track datasets, the individuals
in those datasets, publication status, and the status of any deletion
requests. Researchers will have to do some work to comply with
tracking, but some tools and conventions can make this easier for
them and hopefully boost compliance. Finally, automated systems will
monitor the datasets, help coordinate deletions, and try to flag
untracked datasets.

# Database

## System

Any relational database should be sufficient for this. It should be
able to handle the data for millions of individuals and thousands of
datasets. I'd expect PostgreSQL and MySQL (and variants) to work, but
any choice would likely be fine.

## Schema

```
dataset
  dataset_id	integer	(primary key)
  dataset_type	enum	
  metadata		blob
  status		enum
```
This tracks all datasets. `dataset_type` is used to indicate how the
dataset is stored. For example, it might indicate a file on disk, or
a database. The `metadata` column provides information about things
like the dataset's location and owner. `status` tracks if the dataset
is active, deleted, or published.

With the `dataset_type` and `metadata`, it should be possible to
determine how to respond to a deletion request, whether that is
through some automation, or contacting the dataset owner, or some
other mechanism.

```
individual
  individual_id	integer	primary_key
  status	enum
```
This keeps track of the IDs assigned to individuals when their data
is brought into the R&D environment.

Right now, this mostly serves to keep track of all possible individuals
that could appear in other tables, but it could be extended to include
other (non-identifiable) information about the individual.

The `status` enum stores deletion status so that an individual isn't
included in any new datasets after requesting deletion. 

```
dataset_individual
  dataset_id	integer	(references dataset)
  individual_id	integer	(references individual)
  status	enum
```
The composite key `(dataset_id, individual_id)` should be unique.
This tracks the uses of an individual's data in datasets. `status`
records if the individual is active in that dataset, if a deletion
has been requested, or if the deletion has been confirmed.

## Example Queries

These examples illustrate how the system could be used. They're not
meant to be prescriptive or exhaustive.

Get all unpublished datasets containing a specified individual who
hasn't been deleted. These are datasets we'd need to take action on
if we had a deletion request for that individual:

```
SELECT dataset_id
FROM dataset
JOIN dataset_individual ON dataset.dataset_id = dataset_individual.dataset_id
WHERE dataset_individual.individual_id = ?
AND dataset.status NOT IN ('PUBLISHED', 'DELETED')
AND dataset_individual.status != 'DELETED'
```

Given a list of datasets that need action, we'd store the deletion requests
(this could be done in a single query with the above, but I've separated them
here for clarity):

```
UPDATE dataset_individual
SET status = 'DELETE_REQUESTED'
WHERE individual_id = ? and dataset_id = ?
```

Get all deletion requests for a specified dataset. If I were a researcher or
system responsible for a dataset, I'd use this to figure out who to act on:

```
SELECT individual_id
FROM dataset_individual
JOIN dataset ON dataset.dataset_id = dataset_individual.dataset_id
WHERE dataset_individual.dataset_id = ?
AND dataset_individual.status = 'DELETE_REQUESTED'
```

Get a count of publications each individual has appeared in, so we can inform
them if there were cases we couldn't delete:

```
SELECT COUNT(*), dataset_individual.individual_id
FROM dataset
JOIN dataset_individual on dataset.dataset_id = dataset_individual.dataset_id
WHERE dataset.status = 'PUBLISHED'
AND dataset_individual.status = 'ACTIVE'
GROUP BY dataset_individual.individual_id
```

# Tooling

## Tooling for Human-Managed Datasets

In an R&D environment, there needs to be flexibility to create
new datasets, copy/modify existing ones, and use the most
appropriate storage techniques. Most of this work will be done by
researchers, so self-service tooling will be needed.

Given the nature of research, I'm imagining most of these datasets
are files in a filesytem, and I'd target those use cases first.

Initially, this tooling would probably take the form of scripts that
interact directly with the tracking database. They'd take basic
command line arguments and be able to ingest lists of IDs (e.g. from
text files). It could evolve into a full library or even web-based
tooling, eventually. There will need to be tooling for the following:

1. Creating new datasets. Researchers will need to be able to create
   a new, empty dataset, or copy an existing dataset for modification.
   Copying a dataset should verify there are no deleted individuals in
   it (this might happen if copying a published dataset, for example)
2. Recording which individuals are in a dataset. There should be
   functionality for bulk-adding a list of individuals, and for both
   adding and deleting individuals.
3. Retrieving IDs that need to be deleted from a dataset
4. Recording confirmation of deletions
5. Recording publication of a dataset

Once a dataset has been published, the tools should reject further
changes to the individuals in that dataset.

## Tooling for Datasets in Automated Systems

As datasets mature and move out of exploration, they may develop into
automated systems. For example, databases or web services presenting
an API. The tooling to integrate each of these will be specialized per
system, but will still have the same functionality:

1. Any dataset in an automated system will have an entry in the tracking system
2. The automated system must update the tracking system as individuals
   are added and deleted from the dataset
3. The automated system must be able to query for deletion requests, or
   accept pushed deletion requests from the tracking system
4. The automated system must update the tracking system to confirm deletion

Initially, the automated systems could interact directly with the tracking
system database to do this. Eventually, we might want to build an API for interacting with the tracking system to better manage load and remove the
need for systems to know the details of the tracking system's schema.

## Example Human-Managed Dataset Workflow

A researcher might work with a dataset as follows:

1. The researcher identifies dataset 1 as containing the data they need
2. The researcher runs a tool that copies dataset 1 into their project's
   storage and records this new dataset as dataset 2
3. The researcher pares down the dataset, removing some individuals. They
   run a tool that updates the tracking system to update the set of
   individuals in dataset 2
4. Months later, someone requests a data deletion, and the system
   identifies that their data is in dataset 2
5. The researcher gets a notification that they own a dataset with pending
   deletions
6. The researcher runs a tool to tell them which individuals to delete
7. The researcher manually deletes them
8. The researcher runs a tool to confirm deletion of those individuals
9. More months later, the researcher is done and runs a tool to indicate
   the dataset has been published
10. The researcher doesn't receive any more notifications about deletion requests, since the dataset is published

## Example Deletion Workflow

When an individual requests deletion, it would work as follows:

1. The `individual` table is updated to indicate the individual is not to
   be used in future new datasets
2. The `dataset_individual` table is updated to indicate deletion requests
   in all tracked, unpublished datasets
3. Notifications are sent out, either to researchers or automated systems
4. Automated systems responding to a push notification, or simply on a
   routine basis, query the tracker and discover who they need to delete.
   They do this, and confirm the deletion with the tracker
5. Researchers receive a notification, and run a tool to tell them who to
   delete. They manually do this, and then run a tool to confirm they did
   the deletion.
6. Any deletions in a published dataset are ignored
7. Once all the deletions have been confirmed, the individual is informed
   and possibly told which datasets they could not be deleted from

# Systems to Aid Tracking

I've already hinted at some of these, but a few services will help ensure compliance:

## Notifications of Deletion Requests

When new deletion requests come in, and a researcher is responsible
for managing an affected dataset, they should receive a notification
rather than having to check periodically.

This could be as simple as a script that runs on a routine interval
that queries for datasets with pending deletion requests. If those
datasets are of a manually managed type, the system would use the
dataset metadata to inform the owner.

Email, messaging systems (like Slack), or cloud-managed notification
systems (like AWS SNS) could handle delivering the notifications.

This would be stateless, so a researcher would continue to get notified
until they confirmed deletion.

## Rogue Datasets on Filesystems

It's easy to copy files on a file system, and this will lead to
un-tracked copies of existing datasets. There may also be rogue
datasets that have no tracked copies. It will be impossible to
reliably identify all of these, but we can try to mitigate them.

### Conventions

Asking the researchers to follow some conventions could help:

File-based datasets should be tracked at the directory level, not
at the file level. This makes it more likely intermediate files, or
new files with derived results, still end up under the correct tracking.

Inserting a metadata file into each dataset directory would further
help identify rogue copies (that is, made by a filesystem copy rather than
using the tooling above). The metadata file would describe the dataset and
would contain the dataset ID so it could be found in the tracker.

Finally, it would be useful for researchers to try to put their datasets
into conventional locations. For example, their home directory or a shared
`datasets` directory. This will narrow the search space automated tools
have to consider.

### Scanning for out-of-place copies

With the above coventions in place, a simple script could scan
over the conventional dataset storage locations on a regular basis
and make sure datasets (as indicated by their metadata files) are
in their correct locations (as recorded in the dataset `metadata`
column in the database). If a dataset is detected out of place, it
could trigger an alert, restrict access to the copy, outright
delete it, or take other actions.

### Scanning for untracked datasets

Completely untracked datasets are going to be harder to identify.

Again, our conventions help. Similar to above, the scanning could
look through the "conventional" dataset storage locations and act
(to alert, restrict access, etc.) when it finds directories missing
metadata files.

Finding datasets outside of conventional locations is going to be
tougher. There are a few approaches that might be worth trying.

* The scan could look for large files. This could trigger false
  positives on things like public datasets or software packages, but
  might still be useful with an "ignore" list
* The scan could look for strings that look like IDs

There are tools to help with the second. While intended to scan over
git repos, tools like [detect-secrets](https://github.com/Yelp/detect-secrets)
can be directed at other files and can search for things like high-entropy
numbers, or match patterns.

If we have full freedom over IDs, we could use strings with some
easy-to-search pattern (for example, `MO_12345678901234`). If we are
limited to integer IDs (maybe because of other systems we have to
interact with), we could still reserve part of the ID for a "magic" number.

### Processes for improving

It is difficult to anticipate every way we could fail to track a
dataset. Therefore, some sort of review process is desireable. If
someone finds an out-of-place or untracked dataset, there should be
some process for reporting it.

To encourage reporting, no one should get in trouble for reporting
a untracked dataset. The priority should be getting it into tracking
or cleaning it up, and investigating how it got there.

Periodically, we'd then want to review the reports and see if there
are common patterns. This might suggest further conventions, rules,
or tooling we could set up to keep problems from repeating.

# Stages of Building

It's suggest the following stages for building, trying at each step
to deliver some utility and collect feedback before building additional
functionality. I also suggest focusing on the researcher workflow initially
to get feedback and make sure it's easy for them to comply:

1. Prototyping the database schema and queries. Something like SQLite
   would work for this, and could be used for tests of the workflow
   with scientists.
2. Basic tools for interacting with the tracking system. This would
   enable basics like creating datasets and tracking individuals, and
   again could be used for tests of the workflow. At this point, a
   researcher should be able to manage the entire dataset workflow from
   creation to publication, including deletions.
3. Set up a non-prototype database and adapt the tools to work with it.
   Now there should be one central source of truth for human-managed
   datasets.
4. Add notifications for deletions. At this point, researchers should be
   comfortably using the system, and they should be able to rely on it to
   keep them compliant.
5. Improve the tools based on researcher feedback, and add functionality
   that's missing and quality of life improvements.
6. Build and automate the monitoring for rogue datasets
7. Have other systems start tracking their datasets with this system as
   well, building tooling as needed.