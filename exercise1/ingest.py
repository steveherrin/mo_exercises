#!/usr/bin/env python
"""
Ingest a VCF file. Extract chromosome, position, and allele
frequencies to a pickle

Usage:
  ingest.py <input_VCF> <output_pickle>

Options:
  -h --help     Show this
"""
import gzip
import pickle

import docopt


def parse_vcf(f):
    """
    Given a file-like object for a VCF, extract allele frequencies

    Parameters:
      f: any file-like object

    Returns:
      a generator of ((chromosome, position), allele_frequency) tuples
    """
    for line in f:
        line = line.rstrip()  # strip off trailing newline and any other junk
        if not line or line.startswith('#'):  # skip comments and empty lines
            continue

        # split into the fields the VCF spec requires, plus ignore any sample data after INFO
        chrom, pos, _, _, _, _, _, info, *_ = line.split('\t')

        pos = int(pos)

        info_items = info.split(';')
        info_dict = dict(item.split('=') for item in info_items)
        allele_freq = float(info_dict['AF'])

        yield (chrom, pos), allele_freq


def ingest_file(file):
    """
    Given a VCF file, ingest and extract the data

    Parameters:
      file: string

    Returns:
      dict of {(chromosome, position): allele_frequency, ...}
    """
    if file.endswith('.gz'):  # automatically handle gzipped vcfs
        opened_file = gzip.open(file, mode='rt', encoding='ascii')
    else:
        opened_file = open(file, mode='r', encoding='ascii')

    return {chrpos: af for chrpos, af in parse_vcf(opened_file)}


def ingest_and_write(input_file, output_file):
    """
    Given a VCF file, serialize the data within to an output pickle

    Parameters:
      input_file: string
      output_file: string
    """
    data = ingest_file(input_file)
    with open(output_file, 'wb') as f:
        pickle.dump(data, f, protocol=pickle.HIGHEST_PROTOCOL)


if __name__ == '__main__':
    args = docopt.docopt(__doc__)
    ingest_and_write(args['<input_VCF>'], args['<output_pickle>'])
