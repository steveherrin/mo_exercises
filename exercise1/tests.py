#!/usr/bin/env python
"""
Some tests for tricky bits of code
"""
import io
import unittest

import numpy

import analysis
import ingest


VCF_TEXT = """
##fileformat=VCFv4.0
#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	SAMPLE1
1	12345	.	C	CGAGA	100	PASS	AF=0.301544;NS=203	.
3	33333	.	G	GC	100	PASS	AF=0.295146;NS=141	.
"""  # if editing, please be sure to use tabs, not spaces, for separators


class TestVCFParsing(unittest.TestCase):
    def test_parse_vcf(self):
        file = io.StringIO(VCF_TEXT)
        parser = ingest.parse_vcf(file)

        self.assertEqual(
            [
                (('1', 12345), 0.301544),
                (('3', 33333), 0.295146),
            ],
            list(parser)
        )


class TestAnalysis(unittest.TestCase):
    def test_entropy_works(self):
        """
        Test that the entropy-calculating function works
        I care that it doesn't raise or return nan, but exact
        values aren't important
        """
        freqs = numpy.array([
            [0, 0, 0],
            [1, 1, 1],
            [0, 0.5, 0.5],
            [1, 0.5, 0.5],
            [0.6, 0.4, 0.3],
        ])
        self.assertFalse(numpy.any(numpy.isnan(analysis.entropy(freqs))))

    def test_allele_freq_array(self):
        pops_to_afs = {
            'ASN': {
                ('1', 123): 0.3,
                ('2', 555): 0.9,
                ('4', 111): 0.5,
            },
            'AFR': {
                ('1', 123): 0.2,
                ('2', 555): 0.8,
                ('3', 987): 0.6,
            },
        }

        common_variants, allele_freqs = analysis.make_allele_freq_array(pops_to_afs)
        self.assertEqual(
            [('1', 123), ('2', 555)],
            common_variants
        )
        numpy.testing.assert_array_equal(
            [[0.2, 0.3], [0.8, 0.9]],
            allele_freqs
        )

if __name__ == '__main__':
    unittest.main()
